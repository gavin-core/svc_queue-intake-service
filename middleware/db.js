const discovery = require('node-discovery')

const mongo = discovery.mongo

let _db

module.exports = {
  configureDbConnections: cb => {
    const dbPool = new mongo.MongoConnectionPool([{
      key: 'autobot-queue',
      collections: ['queue_items'],
      connected: db => {
        _db = db
        cb()
      }
    }])

    dbPool.on('error', cb)
  },
  getDb: () => _db
}
