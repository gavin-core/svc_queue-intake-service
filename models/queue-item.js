const QueueTask = require('./queue-task')

const QueueItem = function () {
  const _this = this

  if (_this._id) {
    throw new Error('You may not supply your own _id')
  }

  if (!_this.operation) {
    throw new Error('No operation specified - this property is just an identifier of what the item is trying to acheive during it processesing')
  }

  if (!_this.tasks || !_this.tasks.length) {
    throw new Error('No tasks have been specified - this is essential, otherwise the queue has not reason to accept it')
  }

  const tasks = _this.tasks
  _this.tasks = []

  for (let i = 0; i < tasks.length; i++) {
    _this.tasks.push(QueueTask.call(tasks[i], i))
  }

  _this.nextAvailable = +_this.nextAvailable
  _this.parentOperationId = _this.parentOperationId ? _this.parentOperationId.toObjectID() : undefined
  _this.dateCreated = +new Date()
  _this.assigned = false
  _this.success = false
  _this.data = _this.data || {}
  _this.taskData = {}

  return _this
}

module.exports = QueueItem
