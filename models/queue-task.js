const QueueTask = function (index) {
  const _this = this

  if (!_this.id) {
    throw new Error('A task was specified without an id')
  }

  if (!_this.version) {
    throw new Error('A task was specified without a version')
  }

  if (!+_this.version) {
    throw new Error('A task was speficied with an invalid version, only int values accepted (ie: 1)')
  }

  _this._id = new ObjectID()
  _this.version = +_this.version
  _this.index = index
  _this.data = _this.data || {}
  _this.complete = false
  _this.dateStarted = null
  _this.dateCompleted = null
  _this.delay = +_this.delay || null

  return _this
}

module.exports = QueueTask
