const core = require('node-core')
const discovery = require('node-discovery')
const web = require('node-web')
const readline = require('readline')
const db = require('./middleware/db')
const routes = require('./routes')
const QueueClient = require('./modules/queue-client')

const Discovery = discovery.Discovery
const logger = core.logger
const servicePackage = discovery.servicePackage

/****************************************************************************************************************/

/**/
// if (!!core.config.safeMode || !!process.isChildProcess)
process.on('uncaughtException', err => {
  console.log('uncaught Exception:')
  console.log(err)
  if (err.stack) {
    console.log(err.stack)
  }
})
/**/

/****************************************************************************************************************/
// SAFE EXITING

const _onExit = () => {
  logger.verbose(`${servicePackage.name} has been asked to terminate`)

  if (process.askedToTerminate) {
    return
  }

  process.askedToTerminate = true

  _tryKill()
  setInterval(_tryKill, 5000)
}

const _tryKill = () => {
  logger.verbose(`${servicePackage.name} terminating`)
  process.exit()
}

process.on('SIGINT', _onExit)
    .on('SIGTERM', _onExit)
    .on('SAFE_KILL', _onExit)

if (process.platform === 'win32') {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.on('SIGINT', () => {
    process.emit('SIGTERM')
  })
}

/****************************************************************************************************************/

const configureWebApp = (app, queueClient) => {
  let reqNum = 0
  app.use((req, res, next) => {
    console.log(`${++reqNum}: Request received: ${req.path} (${req.method})`)
    req.queueClient = queueClient
    next()
  })
  app.use(web.sendexpressResponder())

  app.get('/', (req, res) => {
    res.send('<h1>Queue Intake Service</h1>')
  })
  app.get('/ping', (req, res) => {
    res.sendexpress.success('ping')
  })
  app.get('/version', (req, res) => {
    res.sendexpress(null, discovery.servicePackage.version)
  })

  app.post('/items', routes.queueItem.add)

  app.use((req, res) => {
    res.sendexpress.err('Unsupported request')
  })
};

/****************************************************************************************************************/

[
  function discover (next, data) {
    Discovery.discover((err, discInfo) => {
      if (err) {
        return next(err)
      }

      logger.debug(`${servicePackage.name} starting on ${new Date()}`)
      data.discInfo = discInfo
      next()
    })
  },
  db.configureDbConnections,
  function connectToQueue (next, data) {
    data.queueClient = new QueueClient(err => {
      if (err) {
        return next(err)
      }
      next()
    })
  },
  function startWebServer (next, data) {
    web.createServer({
      host: process.network.myAddress
    }, function confWebApp (app) {
      configureWebApp(app, data.queueClient)
    }, function onWebServerListening (err, webInfo) {
      if (err) {
        return next(err)
      }

      logger.info(`${webInfo.protocol}://${webInfo.host}:${webInfo.port} listening`)

      data.interfaces.web = webInfo
      next(null, data)
    })
  },
  function registerWithDiscovery (next, data) {
    Discovery.register({}, data.interfaces, next)
  }
].callInSequence(function (err) {
  if (err) {
    logger.error(err)
    return _onExit()
  }
}, {
  interfaces: {}
})

/****************************************************************************************************************/
