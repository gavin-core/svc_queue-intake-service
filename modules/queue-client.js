const core = require('node-core')
const discovery = require('node-discovery')
const networking = require('node-network')

const Discovery = discovery.Discovery
const logger = core.logger
const servicePackage = discovery.servicePackage
const Version = core.Version
const _ = core._

const QueueClient = function (readyCb) {
  let onReadyCb = () => {
    readyCb = readyCb || (() => {})
    readyCb()
    readyCb = () => {}
  }

  const _this = this
  let _client

  _this.connected = false

  _this.tryConnect = cb => {
    cb = cb || (() => {})
    if (_this.connected) {
      return cb()
    }

    _client = null

    Discovery.getService('queue-service', servicePackage.services['queue-service'].version, (err, service) => {
      if (err || !service) {
        return cb()
      }

      const _interface = _.findWhere(service.interfaces, { key: 'tcp' })
      if (!_interface) {
        throw new Error('Coding error: queue-service doesn\'t implement a \'tcp\' interface')
      }

      _client = new networking.SocketClient({ host: _interface.address, port: _interface.port }, {}, ['REGISTER', 'NEW_ITEM'])
      _client.on('connect', () => {
        _this.connected = true
        logger.debug('Established connection to queue-service')
        _client.rpcs.register({ type: 2 }, cb)
      })

      _client.on('error', err => {
        console.log('Error received but not catered for')
        console.error(err)
      })

      _client.on('disconnect', () => {
        logger.debug('Disconnected from Queue Service')
        _this.connected = false
        _client = null
        _this.tryConnect()
      })
    })
  }

  _this.tryConnect(onReadyCb)
  Discovery.on('service-registered', service => {
    if (_this.connected || !service.name === 'queue-service' || new Version(service.version).matches(servicePackage.services['queue-service'].version)) {
      return
    }

    _this.tryConnect()
  })

  _this.rpcs = {
    newItem: (data, cb) => {
      cb = cb || (() => {})

      if (!_client) {
        return cb()
      }

      _client.rpcs.newItem(data, cb)
    }
  }

  return _this
}

module.exports = QueueClient
