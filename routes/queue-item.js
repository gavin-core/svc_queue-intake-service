const core = require('node-core')
const db = require('../middleware/db')
const models = require('../models')

const QueueItem = models.QueueItem
const _ = core._

exports.add = (req, res) => {
  const queueItems = []

  try {
    if (req.data.items && req.data.items instanceof Array) {
      for (let i = 0; i < req.data.items.length; i++) {
        queueItems.push(QueueItem.call(req.data.items[i]))
      }
    } else {
      queueItems.push(QueueItem.call(req.data))
    }
  } catch (err) {
    return res.sendexpress.preconditionFailed(err.message)
  }

  db.getDb().queue_items.insert(queueItems, (err, items) => {
    if (err) {
      return res.sendexpress(err)
    }

    res.sendexpress(null, _.pluck(items, '_id'))

    for (var i = 0; i < items.length; i++) {
      req.queueClient.rpcs.newItem(items[i]._id)
    }
  })
}
